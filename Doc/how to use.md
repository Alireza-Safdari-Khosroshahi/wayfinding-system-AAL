![](DAI.jpg)
LEDs guide in and through the rooms of the ZE-KI. Individually, according to the requirements of users and user groups.

# How to RUN

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

# important files
main function of project
```
Wegeleitsystem/Code/src/main.js
```
Backend folder to configure project see config.js in this folder
```
Wegeleitsystem/Code/src/components/backend
```
behavior of NavBar
```
Wegeleitsystem/Code/src/components/NavBar.vue
```
behavior of photo carousel
```
Wegeleitsystem/Code/src/components/PhotoCarousel.vue
```
behavior of simple keyboard
```
Wegeleitsystem/Code/src/components/SimpleKeyboard.vue
```
behavior of user buttons
```
Wegeleitsystem/Code/src/components/UserButtons.vue
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


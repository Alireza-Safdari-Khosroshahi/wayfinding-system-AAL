/*******************************************************************************
* Distributed Artificial Intelligence Laboratory TU Berlin.
* Wayfinding system (BACKEND & FRONTEND TEAM)
* All Rights Reserved.
********************************************************************************
* Filename    : main.vue
* Description : 
*
* History
*-------------------------------------------------------------------------------
* Date                       Name                    Description of Change
*
*
*-------------------------------------------------------------------------------
* Developers :  
                Louis Leon Ankel (@ltrou_7)
                Markus Thunig (@markust)
                Adrian Benjamin Brag (@adri.b.brag)
*******************************************************************************/
import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false
new Vue({
  render: h => h(App),
}).$mount('#app')

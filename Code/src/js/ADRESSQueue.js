var ADRESSQueue = {
    queue: [],
    constructor() {
        this.queue = [];
    },

    add(json) {
        this.queue.push(json);
    },

    processNext() {
        if (this.queue.length > 0) {
            let adress = this.queue.shift();

            // To Do: Send it to the Controller
            //POST JSON
            // console.log(adress);
            return adress;
        }
        else{
            console.log("No JSON to send");
        }
    },
    getQueue(){
        return this.queue;
    },

    isEmpty() {
        return this.queue.length === 0;
      },
}

export default ADRESSQueue;


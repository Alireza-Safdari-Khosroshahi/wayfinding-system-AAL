/*******************************************************************************
* Distributed Artificial Intelligence Laboratory TU Berlin.
* Wayfinding system (BACKEND TEAM)
* All Rights Reserved.
********************************************************************************
* Filename    : config.js
* Description : configuration file. This file contains all important settings. 
* please do not change if you do not understand something.
*
* History
*-------------------------------------------------------------------------------
* Date                       Name                    Description of Change
* 15.12.2022                 Alireza                 create file
* 17.12.2022                 Alireza                 Add Json part
* 2.1.2023                   Tobias                  config ESPs
* 07.02.2021                 Tobias                  add Initial JSON
*-------------------------------------------------------------------------------
* Developers :  Alireza Safdari (@mhsafdari)
                Yannic Stoer (@yannicstoer)
                Tobias Schulz (@tobi-study)
                Mier Barsanjy (@mier.barsanjy)
*******************************************************************************/

/* eslint-disable */

var config = {
    // to keep used LEDs in the stack -> in s
    sleepTime:                     30, 

    // for waiting when the LEDs are used -> in s
    DelayTime:                      2,  

    UserColors:{
        User1:                          'e1e0a0',
        User2:                          'FF0000',
        User3:                          '00FF00',
    },

    ESPs:{
        Test:                       ['http://main.local/json/state','http://wled-005.local/json/state'],
        ExperienceHub:              ['http://main.local/json/state'],
        WCHerren:                   ['http://main.local/json/state','http://wcherren.local/json/state'],
        WCFrauen:                   ['http://main.local/json/state','http://wcdamen.local/json/state'],
        ManagementBeuro:            ['http://main.local/json/state','http://management.local/json/state'],        
        FocusSpace:                 ['http://main.local/json/state','http://focusspace.local/json/state'],        
        DesignThinkingSpace:        ['http://main.local/json/state','http://designspace.local/json/state'],        
        CoWorkingSpace:             ['http://main.local/json/state','http://coworkingspace.local/json/state'],        
        LieferroboterTeleoperator:  ['http://main.local/json/state','http://robot2.local/json/state'],        
        Lieferroboterraum:          ['http://main.local/json/state','http://robot1.local/json/state'],        
        WCUnisex:                   ['http://main.local/json/state','http://wcunisex.local/json/state'],        
        Küche:                      ['http://main.local/json/state', 'http://kueche.local/json/state'],
        Technikraum:                ['http://main.local/json/state','http://server.local/json/state'],
        Konferenzraum:              ['http://main.local/json/state','http://konferenzraum.local/json/state'],
    },
    JSONs:{
        WCHerren:                   [{"mainseg": 0, "seg": [{"id":6,  "col": [,,],"sel": true}, {"id":7,  "col": [,,],"sel": true}, {"id":8,  "col": [,,],"sel": true}, {"id":9,  "col": [,,],"sel": true}, {"id":10,  "col": [,,],"sel": true}, {"id":11,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{ "on": true, "start":0,"stop":100,  "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        WCFrauen:                   [{"mainseg": 0, "seg": [{"id":6,  "col": [,,],"sel": true}, {"id":7,  "col": [,,],"sel": true}, {"id":8,  "col": [,,],"sel": true}, {"id":9,  "col": [,,],"sel": true}, {"id":10,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{ "on": true, "start":0,"stop":100,  "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        ExperienceHub:              [{"mainseg": 0, "seg": [{"id":6,  "col": [,,],"sel": true}, {"id":7,  "col": [,,],"sel": true}, {"id":8,  "col": [,,],"sel": true}, {"id":9,  "col": [,,],"sel": true}, {"id":10,  "col": [,,],"sel": true}, {"id":11,  "col": [,,],"sel": true}, {"id":12,  "col": [,,],"sel": true}]}],
        Konferenzraum:              [{"mainseg": 0, "seg": [{"id":2,  "col": [,,],"sel": true}, {"id":3,  "col": [,,],"sel": true}, {"id":4,  "col": [,,],"sel": true}, {"id":5,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{"col": [,,], "sel": true,"rev": true}]}],
        ManagementBeuro:            [{"mainseg": 0, "seg": [{"id":6,  "col": [,,],"sel": true}, {"id":7,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{"col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        FocusSpace:                 [{"mainseg": 0, "seg": [{"id":6,  "col": [,,],"sel": true}, {"id":7,  "col": [,,],"sel": true}, {"id":8,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{ "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": false}]}],
        DesignThinkingSpace:        [{"mainseg": 0, "seg": [{"id":6,  "col": [,,],"sel": true}, {"id":7,  "col": [,,],"sel": true}, {"id":8,  "col": [,,],"sel": true}, {"id":9,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{ "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        CoWorkingSpace:             [{"mainseg": 0, "seg": [{"id":6,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{ "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": false}]}],
        LieferroboterTeleoperator:  [{"mainseg": 0, "seg": [{"id":3,  "col": [,,],"sel": true}, {"id":4,  "col": [,,],"sel": true}, {"id":5,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{   "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        Lieferroboterraum:          [{"mainseg": 0, "seg": [{"id":5,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{   "col": [,,], "fx": 37, "sx": 128, "ix": 128, "sel": true, "rev": true}]}],
        WCUnisex:                   [{"mainseg": 0, "seg": [{"id":4,  "col": [,,],"sel": true}, {"id":5,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{   "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        Küche:                      [{"mainseg": 0, "seg": [{"id":0,  "col": [,,],"sel": true}, {"id":1,  "col": [,,],"sel": true}, {"id":2,  "col": [,,],"sel": true}, {"id":3,  "col": [,,],"sel": true}, {"id":4,  "col": [,,],"sel": true}, {"id":5,  "col": [,,],"sel": true}]}, {"mainseg": 0, "seg": [{ "start":82,"stop":150,  "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        Technikraum:                [{"mainseg": 0, "seg": [{"id":3,  "col": [,,],"sel": true}, {"id":4,  "col": [,,],"sel": true}, {"id":5,  "col": [,,],"sel": true}]}, { "mainseg": 0, "seg": [{   "col": [,,], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        Initial:                    [{"on":true,"bri":126,"transition":7,"ps":-1,"pl":-1,"nl":{"on":false,"dur":60,"mode":1,"tbri":0,"rem":-1},"udpn":{"send":false,"recv":true},"lor":0,"mainseg":0,"seg":[
                                    {"id":0,   "start":0,   "stop":0,    "len":5,   "grp":1,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":240,   "ix":128,   "pal":0,    "sel":true,  "rev":true,"mi":false},
                                    {"id":1,   "start":0,   "stop":12,   "len":7,   "grp":1,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":true,"mi":false},
                                    {"id":2,   "start":12,  "stop":53,   "len":40,  "grp":1,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":true,"mi":false},
                                    {"id":3,   "start":53,  "stop":75,   "len":20,  "grp":1,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":true,"mi":false},
                                    {"id":4,   "start":75,  "stop":109,  "len":34,  "grp":1,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":true,"mi":false},
                                    {"id":5,   "start":109, "stop":153,  "len":66,  "grp":2,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":true,"mi":false},
                                    {"id":6,   "start":153, "stop":223,  "len":48,  "grp":2,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":false,"mi":false},
                                    {"id":7,   "start":223, "stop":244,  "len":21,  "grp":2,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":false,"mi":false},
                                    {"id":8,   "start":244, "stop":273,  "len":29,  "grp":2,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":false,"mi":false},
                                    {"id":9,   "start":273, "stop":315,  "len":42,  "grp":2,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":false,"mi":false},
                                    {"id":10,  "start":315, "stop":345,  "len":30,  "grp":2,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":false,"mi":false},
                                    {"id":11,  "start":345, "stop":388,  "len":43,  "grp":2,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":false,"mi":false},
                                    {"id":12,  "start":388, "stop":404,  "len":16,  "grp":2,    "spc":0,    "of":0,     "on":true,  "frz":false,    "bri":255,  "cct":127,  "col":[0x000000,0x000000,0x000000], "fx":54,    "sx":255,   "ix":128,   "pal":0,    "sel":true,  "rev":false,"mi":false},]}, 
                                    { "mainseg": 0, "seg": [{ "on": true, "start":0,"stop":82, "bri":255,  "col": [0x000000,0x000000,0x000000], "fx": 54, "sx": 255, "ix": 128, "sel": true, "rev": true}]}],
        // off:                        [{"mainseg": 0, "seg": [{"id":2,  "col": [0x000000,,],"sel": true}, {"id":3,  "col": [0x000000,,],"sel": true}, {"id":4,  "col": [0x000000,,],"sel": true}, {"id":5,  "col": [0x000000,,],"sel": true}, {"id":6,  "col": [0x000000,,],"sel": true}, {"id":7,  "col": [0x000000,,],"sel": true}, {"id":8,  "col": [0x000000,,],"sel": true}, {"id":9,  "col": [0x000000,,],"sel": true}, {"id":10,  "col": [0x000000,,],"sel": true}, {"id":11,  "col": [0x000000,,],"sel": true}, {"id":12,  "col": [0x000000,,],"sel": true}, {"id":13,  "col": [0x000000,,],"sel": true}]}, {"mainseg": 0, "seg": [{"col": [0x000000,0x000000,0x000000],"sel": true}]}],
    },

    // for system-initialize 
    init:{
        initJSON:                       { "on": true, "bri": 255, "transition": 7, "mainseg": 0, "seg": [{ "id": 0, "start":0,"stop":400,"grp": 1, "spc": 0, "of": 0, "on": true, "frz": false, "bri": 255, "cct": 127, "col": [0x000000,0x000000,0x000000], "fx": 37, "sx": 128, "ix": 128, "pal": 0, "sel": true, "mi": false }]},
        initESPs:                       ['http://main.local/json/state','http://server.local/json/state','http://wcunisex.local/json/state','http://robot1.local/json/state', 'http://robot2.local/json/state', 'http://coworkingspace.local/json/state', 'http://designspace.local/json/state', 'http://focusspace.local/json/state', 'http://management.local/json/state', 'http://wcdamen.local/json/state','http://wcherren.local/json/state', 'http://kueche.local/json/state','http://konferenzraum.local/json/state'],
    }
  }
  module.exports = config